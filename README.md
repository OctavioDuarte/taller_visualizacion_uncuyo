![](banner.png)


### Fecha: Virtual el día Martes 11 y presencial los día 18, 25 y 27 Junio del 2024.
  
### Lugar: Aula de la cátedra de Tecnología Ambiental Facultad de Ciencias Agrarias
 
## Objetivos

1. Proporcionar herramientas a investigadores de la universidad y del INTA para documentar su trabajo a través de visualizaciones de datos.
1. Proporcionar herramientas para publicar los datos y las visualizaciones generadas de manera efectiva.
1. Ofrecer una introducción a la programación que brinde las herramientas necesarias para poder seguir el curso a los investigadores que sientan que esto es necesario.
1. Sistematizar soluciones a algunos problemas típicos de visualización identificados por las organizaciones involucradas. Usar estas soluciones como casos de estudio y convertirlas en herramientas disponibles para los investigadores.

## Facilitador

**Octavio Duarte** es Especialista en Estadística Matemática y Profesor en Matemática. Sus áreas de trabajo son la estadística, sobre todo orientada a la agroecología y el desarrollo de herramientas abiertas para la manipulación, difusión y modelado de datos e información.

## Herramientas técnicas

* Fuentes de datos (gdocs,surveystack). Que es JSON? Que es una API?
* R: importar datos, basic data wrangling, ggplot2, rmarkdown y potencialmente Shiny.
* JavaScript: D3.js.
* Publicar las visualizaciones (git pipeline/self hosting?)
* Python: a determinar
* Opcional: paseo por la librería TiKZ de LaTeX 


## Modalidad

* Encuentro **virtual** el día martes 11 de junio. Introducción general a la programación previo a la instancia presencial
* 3 encuentros **presenciales** los días 18, 25 y 27 de junio en el aula de la Cátedra de Tecnología Ambiental (ver Cronograma)
* Probablemente queramos un espacio más avanzado para investigar soluciones técnicas más complejas a algunos problemas ya identificados.
  
# Cronograma

## Día 1: Martes 11 de junio
### Modalidad virtual
### Hora: 10 am
### Tema: Introducción general a la programación con R.
* Cómo escribir un guión básico 
* Cómo leer la documentación de una función 
* Cómo encapsular lo que hacemos

## Día 2: Martes 18 de junio
### Lugar: Aula de la cátedra de Tecnología Ambiental FCA UNCuyo
### Hora: 9 a 13
### Tema: Introducción a R. Obtención y preparación de datos.

* Obtener datos de una API. Por ejemplo SurveyStack y GoogleDocs.
* Ver los datos en tablas para revisarlos.
* Formatos. Comparar JSON CSV.
* Gráficos elementales con ggplot.

## Día 3: Martes 25 de junio
### Lugar: Aula de la cátedra de Tecnología Ambiental FCA UNCuyo
### Hora: 9 a 13
### Tema: Gráficos avanzados.

* Gráficos de estadística descriptiva.
* Gráficos de datos geográficos.
* Gráficos interactivos.

## Día 4: Jueves 27 de junio (jornada completa) 
### Lugar: Aula de la cátedra de Tecnología Ambiental FCA UNCuyo
### Hora: 9 a 13
### Tema: Interactividad: Shiny.

### Hora: 1430 a 17
### Tema: Publicación: CI de GitLab y servidor propio.


# Clases Pasadas

## 1 - Introducción a la manipulación de datos

* Primeras manipulaciones y ejemplos de programación, [ clase 1 ](https://octavioduarte.gitlab.io/taller_visualizacion_uncuyo/crecimiento_analisis).

## 2 - Datos tabulares, objetos y primeros gráficos

* Obtener **datos tabulares** de Google Docs, [clase 2 - parte 1](https://octavioduarte.gitlab.io/taller_visualizacion_uncuyo/descargar_datos.html).
* Trabajar con **datos JSON**, por ejemplo SurveyStack [clase 2 - parte 2](https://octavioduarte.gitlab.io/taller_visualizacion_uncuyo/datos_json.html).

## 3 - Introducción a la Integración Continua

* Ejemplo: generar informes en base a archivos en GDrive. [enlace](https://gitlab.com/OctavioDuarte/primer_ejemplo_ci)

## 4 - Mas recursos de Integración Continua

### Usar datos con privacidad

* [Fuente de datos en SurveyStack](https://app.surveystack.io/surveys/667d6b90fc6d3cab820e1c24).

## 5 - Sitios interactivos con Shiny

* [Primeros ejemplos shiny](https://gitlab.com/OctavioDuarte/ejemplo-shiny-taller). En este caso, tenemos dos app en el mismo repositorio.


# Recursos

## Manual en castellano

  Este manual es una lectura recomendadísima. Empieza con una introducción elemental a la programación, tiene ejemplos detallados y ejercicios cortos y relevantes.

* [Manual en castellano](https://es.r4ds.hadley.nz/)

## Machetes (cheatsheets)

* [ Gráficos ](https://rstudio.github.io/cheatsheets/data-visualization.pdf)
* [ Manipulación de datos y tablas ](https://rstudio.github.io/cheatsheets/tidyr.pdf)
* [ Herramientas para aplicar funciones ](https://rstudio.github.io/cheatsheets/purrr.pdf)
* [ Página que engloba todos los tutoriales ](https://rstudio.github.io/cheatsheets/)


# INSCRIPCIÓN
Completar el siguiente formulario:
[Inscripcion](https://app.surveystack.io/surveys/6656235fffb6be2ca533ad8a)

## Organiza

Laboratorio de Tecnologías Libres - FCA - UN Cuyo  
Laboratorio Agroecológico Abierto  
reGOSH  
Our-Sci  

> Financiado por el [Programa Nacional de Ciencia Ciudadana](https://www.argentina.gob.ar/ciencia/sact/ciencia-ciudadana/programa-de-ciencia-ciudadana)
